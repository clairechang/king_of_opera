from sys import exit

import pygame ,math
import Event 
from Event.base_event import BaseEvent

from pygame.locals import *
import Config.event_config as EVENT_CON
import Config.env_config as ENV_CON
import Tools.Img as Img

class Thing(pygame.sprite.Sprite):
    def __init__(self,position,m):
        pygame.sprite.Sprite.__init__(self)
        self.image = m
        self.rect = self.image.get_rect()
        self.rect.topleft = position

class EventSkill(BaseEvent):
    def __init__(self, env, ms):
        self.env = env
        self.priority = ms
        self.surf = env["screen"]
    def do_action(self, browser, skill_type):
        #browser = "firefox"
        #skill_type = 0

        sound = pygame.mixer.Sound('sounds/skill.wav')
        sound.play()

        self.env["gamec"].pause()
        self.env["uic"].pause()
        image2 = Img.skill_firefox
        t = 0
        #skill back
        #judge
        if browser == "firefox":
            image2 = Img.skill_firefox
        if browser == "chrome":
            image2 = Img.skill_chrome
        if browser == "safari":
            image2 = Img.skill_safari
        if browser == "opera":
            image2 = Img.skill_opera
        if browser == "ie":
            image2 = Img.skill_ie    
        #
        ori_skbg = Img.firefox_back
        if browser == "firefox":
            ori_skbg = Img.firefox_back.copy()
        if browser == "chrome":
            ori_skbg = Img.chrome_back.copy()
        if browser == "safari":
            ori_skbg = Img.safari_back.copy()
        if browser == "opera":
            ori_skbg = Img.opera_back.copy()
        #if browser == "ie":
        #    image2 = Img.ie_back.copy()

        #
        image3 = Img.skill[skill_type]
        image = Img.eye5
        #
        ori = self.surf.copy()
        back = self.surf.copy()

        tick = 5

        b_group = pygame.sprite.Group()
        g1 = pygame.sprite.Group()
        g2 = pygame.sprite.Group()
        x = 400
        y = 75
        for i in range(20):
            b_group.clear(self.surf, back)
            b_group.empty()
            black = Img.black.copy()
            black.set_alpha((2*i*255)//100)
            sp = Thing((0,0),black)
            b_group.add(sp)
            b_group.draw(self.surf) 
            
            pygame.display.update()
            pygame.time.wait(tick)


        back = self.surf.copy()
        k = ori_skbg.get_height()//2
        for i in range(10):
            b_group.clear(self.surf, back)
            b_group.empty()
            skbg = ori_skbg.subsurface((0, 65, ori_skbg.get_width(), 10))
            sp = Thing((92.5*(10-i), 270 - 5),skbg)
            b_group.add(sp)
            b_group.draw(self.surf) 
            
            pygame.display.update()
            pygame.time.wait(tick)
        
        skbg = ori_skbg.copy()
        for i in range(10,45):
            g1.clear(skbg, ori_skbg)
            g1.empty()
            g2.clear(skbg, ori_skbg)
            g2.empty()
            b_group.clear(self.surf, back)
            b_group.empty()
            t += 1
            t = t%3
            x -= (50-i)//4
            sp1 = Thing((x-image[t].get_width()/2, y-image[t].get_height()/2-30),image[t])
            sp2 = Thing((x-image2.get_width()/2, y-image2.get_height()/2+50),image2)
            sp3 = Thing((x-image3.get_width()/2+350, y-image3.get_height()/2),image3)
            g1.add(sp1)
            g2.add(sp2)
            g2.add(sp3)
            g2.draw(skbg)
            g1.draw(skbg)
            if i*4 <= 150:
                tskbg = skbg.subsurface((0, k-i*2, skbg.get_width(), i*4))
                sp = Thing((0, 270 - i*2),tskbg)
            else :
                tskbg = skbg
                sp = Thing((0, 270 - k),tskbg)
            b_group.add(sp)
            b_group.draw(self.surf) 
            
            pygame.display.update()
            pygame.time.wait(tick*2)

        for i in range(12):
            g1.clear(skbg, ori_skbg)
            g1.empty()
            g2.clear(skbg, ori_skbg)
            g2.empty()
            b_group.clear(self.surf, back)
            b_group.empty()
            t += 1
            t = t%3
            x -= i*2+5 
            sp1 = Thing((x-image[t].get_width()/2, y-image[t].get_height()/2-30),image[t])
            sp2 = Thing((x-image2.get_width()/2, y-image2.get_height()/2+50),image2)
            sp3 = Thing((x-image3.get_width()/2+350, y-image3.get_height()/2),image3)
            g1.add(sp1)
            g2.add(sp2)
            g2.add(sp3)
            g2.draw(skbg)
            g1.draw(skbg)

            tskbg = skbg.subsurface((0, (i*25)/4, skbg.get_width(), 150*(11-i)/12))
            sp = Thing((0, 270+(i*25)/4-75),tskbg)

            b_group.add(sp)
            b_group.draw(self.surf) 
            pygame.display.update()
            pygame.time.wait(tick)

        self.surf.blit(ori, (0,0))
        pygame.display.update()
        self.env["gamec"].resume()
        self.env["uic"].resume()
