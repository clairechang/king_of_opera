class BaseCard():
    def __init__(self, dic):
        self.position = dic["position"]
        self.r_x = dic["r_x"]
        self.r_y = dic["r_y"]
        self.env = dic["env"]

    def get_center(self):
        return self.position['x'], self.position['y']
