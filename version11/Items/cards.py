import random

from Items.card_base import BaseCard
from Event.base_event import BaseEvent

import Config.game_config as GAME_CON
import Config.event_config as EVENT_CON

import Event

class MassAddingCard(BaseCard):
    def __init__(self, dic, cardtype):
        BaseCard.__init__(self, dic)
        self.cardtype = cardtype
        if self.cardtype == 1:
            self.factor = 2
        else:
            self.factor = 1
        #print("mass adding :", self.factor)

    def trigger(self, player):
        player.char.magic_mass(self.factor*GAME_CON.CARD_AFFECT_TIME) # TEMP
        self.env["gamec"].add_event(EventRadiusAddingChecking(self.env, player, 1))


class EventRadiusAddingChecking(BaseEvent):
    def __init__(self, env, player, ms):
        self.env = env
        self.player = player
        self.priority = ms

    def do_action(self):
        OK = True
        for other_player in self.env["player"]:
            if other_player is self.player: continue
            x1, y1 = other_player.char.position['x'], other_player.char.position['y']
            x2, y2 = self.player.char.position['x'], self.player.char.position['y']
            dist = (other_player.char.radius + self.player.char.get_larger_radius())
            if (x1 - x2)**2 + (y1 - y2)**2 < dist**2 :
                OK = False
                break

        if(OK):
            self.player.char.magic_radius(GAME_CON.CARD_AFFECT_TIME)
        else:
            self.env["gamec"].add_event(EventRadiusAddingChecking(self.env, self.player, self.priority + EVENT_CON.TICKS_PER_TURN*5)) 


class MassMinusingCard(BaseCard):
    def __init__(self, dic, cardtype):
        BaseCard.__init__(self, dic)
        self.factor = 1
        self.cardtype = 1
        #print("mass minusing :", self.factor)

    def trigger(self, player):
        player.char.magic_mass(-self.factor*GAME_CON.CARD_AFFECT_TIME)
        player.char.magic_radius(-self.factor*GAME_CON.CARD_AFFECT_TIME)


class SpeedAddingCard(BaseCard):
    def __init__(self, dic, cardtype):
        BaseCard.__init__(self, dic)
        self.cardtype = cardtype
        if self.cardtype == 1:
            self.factor = 2
        else:
            self.factor = 1
        #print("speed adding :", self.factor)

    def trigger(self, player):
        player.char.magic_acc(self.factor*GAME_CON.CARD_AFFECT_TIME)


class SpeedMinusingCard(BaseCard):
    def __init__(self, dic, cardtype):
        BaseCard.__init__(self, dic)
        self.cardtype = 1
        self.factor = 1
        #print("speed minusing :", self.factor)

    def trigger(self, player):
        player.char.magic_acc(-self.factor*GAME_CON.CARD_AFFECT_TIME)


class ScoreCard(BaseCard):
    def __init__(self, dic, cardtype):
        BaseCard.__init__(self, dic)
        self.cardtype = cardtype
        if self.cardtype == 1:
            self.factor = 2
        else:
            self.factor = 1
        #print("score :", self.factor)

    def trigger(self, player):
        player.add_score(self.factor*GAME_CON.CARD_ADD_SCORE)
