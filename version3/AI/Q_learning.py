class Q_learning():

    def __init__(self, a, g):
        self.Q_table = [[0] * 4] * 19
        #回中間, 衝A, 衝B, 衝C
        self.alpha = a
        self.gamma = g

    def get_action(self, state):
        return self.Q_table[state].index(max(self.Q_table[state]))
    
    def update(self, prev_state, prev_action, cur_state, reward):
        maxQ = max(self.Q_table[cur_state])
        self.Q_table[prev_state][prev_action] = self.Q_table[prev_state][prev_action] + self.alpha * (reward + self.gamma * maxQ - self.Q_table[prev_state][prev_action])
