import pygame

normal_alpha = 255*0.8

#read background and bound
Background = pygame.image.load("screen/bg-light.jpg")
Bound = pygame.image.load("screen/circle/bound.png")
black = pygame.image.load("screen/skill/black.png")
#read wall
Wall = []
for i in range(12):
    index = "screen/circle/wall" + str(i+1) + ".png"
    image = pygame.image.load(index)
    Wall.append(image)

#read icon
Opera = []
for i in range(24):
    index = "screen/icon/opera" + str(i+1) + ".png"
    image = pygame.image.load(index)
    Opera.append(image)

Chrome = []
for i in range(24):
    index = "screen/icon/chrome" + str(i+1) + ".png"
    image = pygame.image.load(index)
    Chrome.append(image)

Safari = []
for i in range(24):
    index = "screen/icon/safari" + str(i+1) +".png"
    image = pygame.image.load(index)
    Safari.append(image)

Firefox = []
for i in range(24):
    index = "screen/icon/firefox" + str(i+1) +".png"
    image = pygame.image.load(index)
    Firefox.append(image)

IE = []
for i in range(24):
    index = "screen/icon/IE" + str(i+1) +".png"
    image = pygame.image.load(index)
    IE.append(image)

b_opera = []
b_chrome = []
b_safari = []
b_firefox = []
b_ie = []
s = 0.63
for i in range(24):
    img1 = pygame.transform.scale(Opera[i],(int(Opera[i].get_width()*s), int(Opera[i].get_height()*s)))
    img2 = pygame.transform.scale(Chrome[i],(int(Chrome[i].get_width()*s), int(Chrome[i].get_height()*s)))   
    img3 = pygame.transform.scale(Safari[i],(int(Safari[i].get_width()*s), int(Safari[i].get_height()*s)))
    img4 = pygame.transform.scale(Firefox[i],(int(Firefox[i].get_width()*s), int(Firefox[i].get_height()*s)))
    img5 = pygame.transform.scale(IE[i],(int(IE[i].get_width()*s), int(IE[i].get_height()*s)))
    b_opera.append(img1)
    b_chrome.append(img2)
    b_safari.append(img3)   
    b_firefox.append(img4)
    b_ie.append(img5)

n_opera = []
n_chrome = []
n_safari = []
n_firefox = []
n_ie = []
s = 0.35
for i in range(24):
    img1 = pygame.transform.scale(Opera[i],(int(Opera[i].get_width()*s), int(Opera[i].get_height()*s)))
    img2 = pygame.transform.scale(Chrome[i],(int(Chrome[i].get_width()*s), int(Chrome[i].get_height()*s)))   
    img3 = pygame.transform.scale(Safari[i],(int(Safari[i].get_width()*s), int(Safari[i].get_height()*s)))
    img4 = pygame.transform.scale(Firefox[i],(int(Firefox[i].get_width()*s), int(Firefox[i].get_height()*s)))
    img5 = pygame.transform.scale(IE[i],(int(IE[i].get_width()*s), int(IE[i].get_height()*s)))
    n_opera.append(img1)
    n_chrome.append(img2)
    n_safari.append(img3)   
    n_firefox.append(img4)
    n_ie.append(img5)

s_opera = []
s_chrome = []
s_safari = []
s_firefox = []
s_ie = []
s = 0.25
for i in range(24):
    img1 = pygame.transform.scale(Opera[i],(int(Opera[i].get_width()*s), int(Opera[i].get_height()*s)))
    img2 = pygame.transform.scale(Chrome[i],(int(Chrome[i].get_width()*s), int(Chrome[i].get_height()*s)))   
    img3 = pygame.transform.scale(Safari[i],(int(Safari[i].get_width()*s), int(Safari[i].get_height()*s)))
    img4 = pygame.transform.scale(Firefox[i],(int(Firefox[i].get_width()*s), int(Firefox[i].get_height()*s)))
    img5 = pygame.transform.scale(IE[i],(int(IE[i].get_width()*s), int(IE[i].get_height()*s)))
    s_opera.append(img1)
    s_chrome.append(img2)
    s_safari.append(img3)   
    s_firefox.append(img4)
    s_ie.append(img5)
'''
for i in range(24):
    b_opera[i].fill((255, 255, 255, normal_alpha), None, pygame.BLEND_RGBA_MULT)
    b_chrome[i].fill((255, 255, 255, normal_alpha), None, pygame.BLEND_RGBA_MULT)
    b_safari[i].fill((255, 255, 255, normal_alpha), None, pygame.BLEND_RGBA_MULT)
    b_firefox[i].fill((255, 255, 255, normal_alpha), None, pygame.BLEND_RGBA_MULT)
    n_opera[i].fill((255, 255, 255, normal_alpha), None, pygame.BLEND_RGBA_MULT)
    n_chrome[i].fill((255, 255, 255, normal_alpha), None, pygame.BLEND_RGBA_MULT)
    n_safari[i].fill((255, 255, 255, normal_alpha), None, pygame.BLEND_RGBA_MULT)
    n_firefox[i].fill((255, 255, 255, normal_alpha), None, pygame.BLEND_RGBA_MULT)
    s_opera[i].fill((255, 255, 255, normal_alpha), None, pygame.BLEND_RGBA_MULT)
    s_chrome[i].fill((255, 255, 255, normal_alpha), None, pygame.BLEND_RGBA_MULT)
    s_safari[i].fill((255, 255, 255, normal_alpha), None, pygame.BLEND_RGBA_MULT)
    s_firefox[i].fill((255, 255, 255, normal_alpha), None, pygame.BLEND_RGBA_MULT)
'''
#read card
Chance = []
Destiny = []
for i in range(2):
    index = "screen/icon/chance1.png"
    img1 = pygame.image.load(index)
    index = "screen/icon/destiny1.png"
    img2 = pygame.image.load(index)
    img1 = pygame.transform.scale(img1, (45,50))
    img2 = pygame.transform.scale(img2, (45,50))
    Chance.append(img1)
    Destiny.append(img2)
for i in range(4):
    index = "screen/icon/chance" + str(i+1) + ".png"
    img1 = pygame.image.load(index)
    index = "screen/icon/destiny" + str(i+1) + ".png"
    img2 = pygame.image.load(index)
    img1 = pygame.transform.scale(img1, (45,50))
    img2 = pygame.transform.scale(img2, (45,50))
    Chance.append(img1)
    Destiny.append(img2)

#read eye
eye = pygame.image.load("screen/eyes/eye1.png")
eye2 = pygame.image.load("screen/eyes/eye2.png")

eye3 = []
s_eye3 = []
n_eye3 = []
b_eye3 = []
eye3_1 = pygame.image.load("screen/eyes/eye3-1.png")
eye3_2 = pygame.image.load("screen/eyes/eye3-2.png")
eye3_3 = pygame.image.load("screen/eyes/eye3-3.png")
eye3_4 = pygame.image.load("screen/eyes/eye3-4.png")
eye3_5 = pygame.image.load("screen/eyes/eye3-5.png")
eye3_6 = pygame.image.load("screen/eyes/eye3-6.png")
eye3.append(eye3_1)
eye3.append(eye3_2)
eye3.append(eye3_3)
eye3.append(eye3_4)
eye3.append(eye3_5)
eye3.append(eye3_6)

eye4 = []
s_eye4 = []
n_eye4 = []
b_eye4 = []
eye4_1 = pygame.image.load("screen/eyes/eye4-1.png")
eye4_2 = pygame.image.load("screen/eyes/eye4-2.png")
eye4_3 = pygame.image.load("screen/eyes/eye4-3.png")
eye4_4 = pygame.image.load("screen/eyes/eye4-4.png")
eye4_5 = pygame.image.load("screen/eyes/eye4-5.png")
eye4_6 = pygame.image.load("screen/eyes/eye4-6.png")
eye4.append(eye4_1)
eye4.append(eye4_2)
eye4.append(eye4_3)
eye4.append(eye4_4)
eye4.append(eye4_5)
eye4.append(eye4_6)

eye5 = []
eye5_1 = pygame.image.load("screen/eyes/eye5-1.png")
eye5_2 = pygame.image.load("screen/eyes/eye5-2.png")
eye5_3 = pygame.image.load("screen/eyes/eye5-3.png")
eye5.append(eye5_1)
eye5.append(eye5_2)
eye5.append(eye5_3)

eye6 = []
eye6_1 = pygame.image.load("screen/eyes/eye6-1.png")
eye6_2 = pygame.image.load("screen/eyes/eye6-2.png")
eye6_3 = pygame.image.load("screen/eyes/eye6-3.png")
eye6.append(eye6_1)
eye6.append(eye6_2)
eye6.append(eye6_3)

skill_chrome = pygame.image.load("screen/skill/chrome.png")
skill_firefox = pygame.image.load("screen/skill/firefox.png")
skill_opera = pygame.image.load("screen/skill/opera.png")
skill_safari = pygame.image.load("screen/skill/safari.png")
skill_ie = pygame.image.load("screen/skill/IE.png")

firefox_back = pygame.image.load("screen/skill/firefoxbg.png")
chrome_back = pygame.image.load("screen/skill/chromebg.png")
safari_back = pygame.image.load("screen/skill/safaribg.png")
opera_back = pygame.image.load("screen/skill/operabg.png")
#ie_back = pygame.image.load("screen/skill/iebg.png")
skill = []
skill1 = pygame.image.load("screen/skill/skill1.png")
skill2 = pygame.image.load("screen/skill/skill2.png")
skill3 = pygame.image.load("screen/skill/skill3.png")
skill4 = pygame.image.load("screen/skill/skill4.png")
skill5 = pygame.image.load("screen/skill/skill5.png")
skill6 = pygame.image.load("screen/skill/skill6.png")
skill7 = pygame.image.load("screen/skill/skill7.png")
skill8 = pygame.image.load("screen/skill/skill8.png")
skill.append(skill1)
skill.append(skill2)
skill.append(skill3)
skill.append(skill4)
skill.append(skill5)
skill.append(skill6)
skill.append(skill7)
skill.append(skill8)


s=0.63
eye_b_size = int(eye.get_width()*s), int(eye.get_height()*s)
b_eye = pygame.transform.scale(eye, (eye_b_size))
b_eye2 = pygame.transform.scale(eye2, (eye_b_size))
for i in range(6):
    temp_image = pygame.transform.scale(eye4[i], (eye_b_size))
    b_eye4.append(temp_image)
for i in range(6):
    temp_image = pygame.transform.scale(eye3[i], (eye_b_size))
    b_eye3.append(temp_image)

s=0.36
eye_n_size = int(eye.get_width()*s), int(eye.get_height()*s)
n_eye = pygame.transform.scale(eye, (eye_n_size))
n_eye2 = pygame.transform.scale(eye2, (eye_n_size))
for i in range(6):
    temp_image = pygame.transform.scale(eye4[i], (eye_n_size))
    n_eye4.append(temp_image)
for i in range(6):
    temp_image = pygame.transform.scale(eye3[i], (eye_n_size))
    n_eye3.append(temp_image)

s=0.25
eye_s_size = int(eye.get_width()*s), int(eye.get_height()*s)
s_eye = pygame.transform.scale(eye, (eye_s_size))
s_eye2 = pygame.transform.scale(eye2, (eye_s_size))
for i in range(6):
    temp_image = pygame.transform.scale(eye4[i], (eye_s_size))
    s_eye4.append(temp_image)
for i in range(6):
    temp_image = pygame.transform.scale(eye3[i], (eye_s_size))
    s_eye3.append(temp_image)
