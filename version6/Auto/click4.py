'''
    team4 ai test
'''
import pygame
from pygame.locals import *
from AI.base_ai import BaseAi
import AI.ai_config
class TeamAI(BaseAi):
    def __init__(self, env):
        BaseAi.__init__(self)
        self.env = env
        self.name = "team4"
    def decide(self, helper):
        #pygame.event.pump()
        if self.env["py4"]:
            return AI.ai_config.ActionPress
        else:
            return AI.ai_config.ActionUnPress

