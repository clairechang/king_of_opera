import math
import random
from AI.base_ai import BaseAi
from AI.ai_config import *
from AI.Q_learning import Q_learning

class TeamAI(BaseAi):
	def __init__(self,env):
		BaseAi.__init__(self)
		self.name = "training"
		self.agent = Q_learning(0.7, 0.3)
		self.prev_action = 1
		self.prev_state = "300300030"
		self.prev_score = 0

	def decide(self, helper):
		if helper.me.is_dead() or helper.me.is_lock():
			return ActionPress

		state = helper.me.get_Qstate(helper.get_others_degree()[0])
		score = helper.me.get_Qscore()
		self.agent.update(self.prev_state, self.prev_action, state, score - self.prev_score)
		self.prev_state = state
		self.prev_score = score

		other = helper.get_others_degree()[0] # 最快會轉到的那隻
		if helper.me.check_facing(other.get_position()):
			action = ActionPress
			#action = random.choice([ActionPress, ActionPress, ActionPress, ActionPress, ActionPress, ActionPress, ActionPress, ActionUnPress])
		else:
			action = ActionUnPress
			#action = random.choice([ActionPress, ActionUnPress, ActionUnPress, ActionUnPress, ActionUnPress, ActionUnPress, ActionUnPress])

		if action == ActionPress:
			self.prev_action = 1
		else:
			self.prev_action = 0

		return action


