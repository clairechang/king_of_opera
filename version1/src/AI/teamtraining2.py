'''
    hh ai test
'''
import math
from AI.base_ai import BaseAi
import AI.ai_config
from AI.Q_learning import Q_learning
import random

class TeamAI(BaseAi):
    def __init__(self,env):
        BaseAi.__init__(self)
        self.name = "training"
        self.EPS = 0.1
        self.click = False
        self.target = None
        self.prev = AI.ai_config.ActionUnPress
        self.click_cd = 0

        self.agent = Q_learning(0.7, 0.99)
        self.prev_action = 1
        self.prev_state = "300300030"
        self.prev_score = 0
        self.count = 0
        self.flag = True

    def do_press(self):
        self.EPS = 0.47
        self.prev = AI.ai_config.ActionPress
        return AI.ai_config.ActionPress
    
    def do_unpress(self):
        self.EPS = 0.1
        self.prev = AI.ai_config.ActionUnPress
        return AI.ai_config.ActionUnPress

    def do_click(self, flag=True):
        if flag:
            if self.click_cd != 0:
                return self.prev
            else:
                self.click_cd = 10;
        self.click = flag
        if self.prev == AI.ai_config.ActionUnPress:
            self.prev = AI.ai_config.ActionPress
            return AI.ai_config.ActionPress
        else:
            self.prev = AI.ai_config.ActionUnPress
            return AI.ai_config.ActionUnPress
        print('click')
        self.click = True
        self.prev = AI.ai_config.ActionPress
        return AI.ai_config.ActionPress

    def check_target(self, target, helper):
        if not target:
            return False

        if target.is_dead():
            return False

        # 避免隔山打牛
        for other in helper.others:
            if other == target: continue
            t_pos = target.get_position()
            o_pos = other.get_position()
            if helper.check_degree_eq(helper.me.cal_vector(t_pos)[1], helper.me.cal_vector(o_pos)[1], 0.47):
                return False

        # 避免撞牆
        for wall in helper.get_walls():
            arc = helper.me.cal_vector(target.get_position())[1]
            r = helper.get_field_radius()
            d1 = helper.me.cal_vector((r*math.cos(wall[0]), r*math.sin(wall[0])))[1]
            d2 = helper.me.cal_vector((r*math.cos(wall[1]), r*math.sin(wall[1])))[1]
            if d1 > d2:
                d1 -= 2*math.pi
                if arc > math.pi:
                    arc -= 2*math.pi
            if d1 < arc and arc < d2:
                return False

        # 避免原地轉太久
        degree = helper.me.cal_rel_degree(target.get_position())
        degree = min(degree, 2*math.pi-degree)
        if degree > math.pi/3:
            return False

        return True

    def get_ok_target(self, helper):
        others = sorted(helper.others, key=lambda x: 
            min(helper.me.cal_rel_degree(x.get_position()), 2*math.pi-helper.me.cal_rel_degree(x.get_position())))
        
        for other in others:
            if self.check_target(other, helper):
                return other

        return None

    def cal_xy(self, vec):
        return (vec[0]*math.cos(vec[1]), vec[0]*math.sin(vec[1]))

    def pre(self, action):
        if self.count < 1000:
            self.count += 1
        else:
            self.count = 0
            if self.flag:
                self.flag = False
                #print(".")
            else:
                self.flag = True
                #print(".")

        if action == AI.ai_config.ActionPress:
            if self.flag:
                self.prev_action = 1
            else:
                self.prev_action = 0
        else:
            if self.flag:
                self.prev_action = 0
            else:
                self.prev_action = 1
        
        if self.prev_action == 1:
            return AI.ai_config.ActionPress
        else:
            return AI.ai_config.ActionUnPress

    def decide(self, helper):
        player = helper.get_others_degree()
        if helper.me.is_dead() or player == []:
            return AI.ai_config.ActionNull

        state = helper.me.get_Qstate(player[0])
        score = helper.me.get_Qscore()
        self.agent.update(self.prev_state, self.prev_action, state, score - self.prev_score)
        self.prev_state = state
        self.prev_score = score

        if self.click_cd != 0:
            self.click_cd -= 1
            
        if self.click:
            return self.pre(self.do_click(False))

        # Attack target
        if not self.check_target(self.target, helper):
            self.target = self.get_ok_target(helper)
        if self.target:
            if helper.me.check_facing(self.target.get_position(), self.EPS*(helper.me.get_speed()[0]//100*0.2+1)):
                return self.pre(self.do_press())
            else:
                degree = helper.me.cal_rel_degree(self.target.get_position())
                if degree > 2*math.pi-degree + 0.5:
                    return self.pre(self.do_click())
                return self.pre(self.do_unpress())

        # Move to center
        if helper.me.get_position_r()[0] > 30:
            if helper.me.check_facing((0, 0), self.EPS*3):
                return self.pre(self.do_press())
            else:
                now_degree = helper.me.cal_rel_degree((0, 0))
                rev_degree = 2*math.pi - now_degree
                if now_degree > rev_degree + 1:
                    return self.pre(self.do_click())

        # Otherwise
        return self.pre(self.do_unpress())
        
