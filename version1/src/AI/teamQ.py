'''
    AI template
'''
from AI.base_ai import BaseAi
from AI.ai_config import *
import csv

class TeamAI(BaseAi):
    #action = 0 is Unpress, action = 1 is Press
    def __init__(self, env):
        BaseAi.__init__(self)
        self.name = "Q-learning"
        self.Q_table = []
        with open('Q_table.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                self.Q_table.append(row)

    def decide(self, helper):
        player = helper.get_others_degree()

        if helper.me.is_dead() or player == []:
            return ActionNull

        state = helper.me.get_Qstate(helper.get_others_degree()[0])
        
        if state not in self.Q_table[0]:
            return ActionNull

        if self.Q_table[0][state] > self.Q_table[1][state]:
            return ActionUnPress
        else:
            return ActionPress
