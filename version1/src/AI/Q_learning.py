import csv
import Config.game_config as GAME_CON

class Q_learning():

    def __init__(self, a, g):
        self.Q_table = [dict([("300300030", 0)]), dict([("300300030", 0)])]
        self.alpha = a
        self.gamma = g

    def get_Q_table(self):
        return self.Q_table

    def write(self):
        print("begin to write dic.")
        with open('Q_table.csv', 'w') as csvfile:
            fieldnames = self.Q_table[0].keys()
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writeheader()
            writer.writerow(self.Q_table[0])
            writer.writerow(self.Q_table[1])
        print("dic written.")

    def update(self, prev_state, prev_action, cur_state, reward):
        #initialize the Q value and update the previous Q value
        #print(reward)
        #print(GAME_CON.WRITE_DIC)
        #print(GAME_CON.TIMEUP)
        #print("-")
        if not GAME_CON.WRITE_DIC:
            if cur_state not in self.Q_table[0]:
                self.Q_table[0][cur_state] = 0
            if cur_state not in self.Q_table[1]:
                self.Q_table[1][cur_state] = 0
            self.Q_table[prev_action][prev_state] = self.Q_table[prev_action][prev_state] + self.alpha * (reward + self.gamma * max(self.Q_table[0][cur_state], self.Q_table[1][cur_state]) - self.Q_table[prev_action][prev_state])
        """
        if GAME_CON.TIMEUP:
            GAME_CON.TIMEUP = False
            GAME_CON.WRITE_DIC = True
            print("begin to write dic.")
            with open('Q_table.csv', 'w') as csvfile:
                fieldnames = self.Q_table[0].keys()
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

                writer.writeheader()
                writer.writerow(self.Q_table[0])
                writer.writerow(self.Q_table[1])
            print("dic written.")
        """
